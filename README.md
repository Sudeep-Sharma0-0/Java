# Learning Git And Java

## Table of Contents

| S.N. 	| Topic                	| Chapters 	|       Status       	| 
|------	|----------------------	|:--------:	|:------------------:	|
|  1.  	| [Basics]             	|    4     	| :heavy_check_mark: 	|
|  2.  	| [Control Statements] 	|    5     	| :heavy_check_mark: 	|

## Assignments

| S.N.  | [Assignments]         | Section   |       Status          |
|------ |---------------------- | :-------: | :------------------:  |
|  1.   | [Fibonacci Series]    |  :x:      | :heavy_check_mark:    |
|  2.   | [Prime Numbers]       |  :x:      | :heavy_check_mark:    |
|  3.   | [Palindrome]          |  :x:      | :heavy_check_mark:    |
|  4.   | [Factorial]           |  :x:      | :heavy_check_mark:    |
|  5.   | [Armstrong]           |   2       | :heavy_check_mark:    |
|  6.   | [Random Number]       |  :x:      | :heavy_check_mark:    |
|  7.   | [Patterns]            |   7       | :heavy_check_mark:    |


[Basics]: https://github.com/Sudeep-Sharma0-0/Java/tree/master/src/Basics
[Control Statements]: https://github.com/Sudeep-Sharma0-0/Java/tree/master/src/Control_Statements

[Assignments]: https://github.com/Sudeep-Sharma0-0/Java/tree/master/src/Assignments
[Fibonacci Series]: https://github.com/Sudeep-Sharma0-0/Java/blob/master/src/Assignments/Basics/One_FibonacciSeries.java
[Prime Numbers]: https://github.com/Sudeep-Sharma0-0/Java/blob/master/src/Assignments/Basics/Two_PrimeNumbers.java
[Palindrome]: https://github.com/Sudeep-Sharma0-0/Java/blob/master/src/Assignments/Basics/Three_Palindrome.java
[Factorial]: https://github.com/Sudeep-Sharma0-0/Java/blob/master/src/Assignments/Basics/Four_Factorial.java
[Armstrong]: https://github.com/Sudeep-Sharma0-0/Java/blob/master/src/Assignments/Basics/
[Random Number]: https://github.com/Sudeep-Sharma0-0/Java/blob/master/src/Assignments/Basics/Six_RandomGen.java
[Patterns]: https://github.com/Sudeep-Sharma0-0/Java/blob/master/src/Assignments/Basics/

