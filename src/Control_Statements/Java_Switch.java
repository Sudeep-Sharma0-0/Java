package Control_Statements;

import java.util.Scanner;

public class Java_Switch {
    public enum day {Sun, Mon, Tue, Wed, Thu, Fri, Sat}
    public static Scanner input = new Scanner(System.in);

    public static void main(String[] args) {
        //Normal Switch...
        System.out.println("\n1: Tea \n2: Coffee \n3: Orange Juice \n4: Milk");
        System.out.println("Enter your option: ");
        int option = input.nextInt();

        switch(option) {
            case 1:
            System.out.println("You Ordered Tea...");
            break;

            case 2:
            System.out.println("You Ordered Coffee...");
            break;

            case 3:
            System.out.println("You Ordered Orange Juice...");
            break;

            case 4:
            System.out.println("You Ordered Milk...");
            break;

            default:
            System.out.println("Please Enter a valid option...");
            break;
        }

        //Enum Switch...
        day today[] = day.values();
        for (day var : today) {
            switch(var) {
                case Sun:
                System.out.println("Sunday");
                break;

                case Mon:
                System.out.println("Monday");
                break;

                case Tue:
                System.out.println("Tueday");
                break;

                case Wed:
                System.out.println("Wednesday");
                break;

                case Thu:
                System.out.println("Thursday");
                break;

                case Fri:
                System.out.println("Friday");
                break;

                case Sat:
                System.out.println("Saturday");
                break;

                default:
                System.out.println("ERROR!!!");
                break;
            }
        }
    }
}
