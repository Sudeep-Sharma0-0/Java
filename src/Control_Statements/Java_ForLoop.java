package Control_Statements;

public class Java_ForLoop {
    public static void main(String[] args) {
        //Normal For Loop...
        for (int i = 0; i < 5; i++) {
            System.out.println(i);
        }

        //Nested For Loop
        for (int i = 0; i < 5; i++) {
            for (int j = 0; j < i; j++) {
                System.out.print("*");
            }
            System.out.println("");
        }
        
        //For Each...
        int arr[] = {12, 13, 14, 15};

        for(int i:arr) {
            System.out.println(i);
        }

        //Infinite Loop
        for(;;) {
            System.err.println("ERROR!!!"); //Press Ctrl + C to exit or press stop in any IDE.
        }
    }
}
