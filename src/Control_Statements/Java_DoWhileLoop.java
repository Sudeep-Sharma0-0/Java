package Control_Statements;

public class Java_DoWhileLoop {
    public static void main(String[] args) {
        int i = 0;
        //Simple Do While
        do {
            System.out.println(i);
            i++;
        } while (i < 5);

        //Infinite Do While
        do {
            System.out.println("Infinite...");
            //Press ctrl + C in terminal or stop running in IDE
        } while (true);
    }
}
