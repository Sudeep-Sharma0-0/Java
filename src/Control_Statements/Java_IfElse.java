package Control_Statements;

import java.util.Scanner;

public class Java_IfElse {
    public static void main(String[] args) {
        int var1, var2, var3;
        Scanner input = new Scanner(System.in);

        System.out.println("Enter two numbers: ");
        var1 = input.nextInt();
        var2 = input.nextInt();

        //If Conditional Statement
        if (var1 > var2) {
            System.out.println(var1 + " is the larger.");
        }

        if (var2 > var1) {
            System.out.println(var2 + " is the larger.");
        }

        //If Else Conditional Statement
        if (var1 > var2) {
            System.out.println(var1 + " is the larger.");
        } else {
            System.out.println(var2 + " is the larger.");
        }

        //If Else If Conditional Statement
        if (var1 > var2) {
            System.out.println(var1 + " is the larger.");
        } else if (var2 > var1) {
            System.out.println(var2 + " is the larger.");
        }

        System.out.println("\nEnter three numbers: ");
        var1 = input.nextInt();
        var2 = input.nextInt();
        var3 = input.nextInt();

        //Nested If Else
        if (var1 > var2) {
            if (var1 > var3) {
                System.out.println(var1 + " is the largest.");
            } else {
                System.out.println(var3 + " is the largest.");
            }
        } else {
            if (var2 > var3) {
                System.out.println(var2 + " is the largest.");
            } else {
                System.out.println(var3 + " is the largest.");
            }
        }

        //Ternary Operator
        int var = 10;
        String output = (var % 2 == 0)?"The number is even." : "The number is odd.";
        System.out.println(output);

        //If Else Ladder
        if (var1 > var2) {
            System.out.println(var1 + " is the larger.");
        } else if (var2 > var1) {
            System.out.println(var2 + " is the larger.");
        } else {
            System.err.println("ERROR!!!");
        }
    }
}
