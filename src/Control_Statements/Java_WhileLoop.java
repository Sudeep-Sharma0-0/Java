package Control_Statements;

public class Java_WhileLoop {
    public static void main(String[] args) {
        int i = 0;

        //Normal While...
        while (i < 5) {
            System.out.println(i);
            i++;
        }

        //Infinite While
        while(true) {
            System.out.println("Infinite!!!");
            //Press ctrl + C in terminal or stop running in IDE.
        }
    }
}
