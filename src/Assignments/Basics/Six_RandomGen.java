package Assignments.Basics;

import java.util.Scanner;

public class Six_RandomGen {
    public static Scanner input = new Scanner(System.in);

    public static void main(String[] args) {
        int min, max, num, ans;
        
        System.out.println("Enter the minimum value: ");
        min = input.nextInt();

        System.out.println("Enter the maximum value: ");
        max = input.nextInt();

        System.out.println("Enter the number of items you want: ");
        num = input.nextInt();
        
        System.out.println("Your random numbers: ");
        
        for (int i = 0; i < num; i++) {
            ans = (int)(Math.random() * (max - min + 1) + min);
            System.out.println(ans);
        }
    }
}
