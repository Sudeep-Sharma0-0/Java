package Assignments.Basics;

import java.util.Scanner;

public class SevenG_Patterns {
    public static Scanner sc = new Scanner(System.in);
    public static void main(String[] args) {
        String input = sc.nextLine().trim();
        char[] data = input.toCharArray();

        for (int i = 0; i < data.length; i++) {
            for (int j = 0; j <= i; j++) {
                System.out.print(data[j]);
            }
            System.out.println();
        }
    }
}
