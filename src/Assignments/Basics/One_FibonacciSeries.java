package Assignments.Basics;

import java.util.Scanner;

public class One_FibonacciSeries {
    public static Scanner input = new Scanner(System.in);
    public static void main(String[] args) {
        int num;
        int a = 0, b = 1, c;

        System.out.println("Enter the number of terms in Fibonacci series: ");
        num = input.nextInt();
        int[] result = new int[num];

        for (int i = 0; i < num; i++) {
            result[i] = a;

            c = a + b;
            a = b;
            b = c;

            System.out.print(result[i] + ", ");
        }
        System.out.println();
    }
}
