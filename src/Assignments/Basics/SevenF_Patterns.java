package Assignments.Basics;

public class SevenF_Patterns {
    public static void main(String[] args) {
        int row = 5;
        for (int i = row; i > 0; i--) {
            for (int j = 0; j < (row - i) ; j++) {
                System.out.print(" ");
            }
            for (int j = i; j > 0; j--) {
                System.out.print("*");
            }
            System.out.println();
        }

        for (int i = row; i > 0; i--) {
            for (int j = 0; j < (row - i) ; j++) {
                System.out.print(" ");
            }
            for (int j = i; j > 0; j--) {
                System.out.print(" *");
            }
            System.out.println();
        }
    }
}
