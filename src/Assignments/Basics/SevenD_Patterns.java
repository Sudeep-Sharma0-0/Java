package Assignments.Basics;

import java.util.Scanner;

public class SevenD_Patterns {
    public static Scanner sc = new Scanner(System.in);
    public static void main(String[] args) {
        int row = sc.nextInt();
        for (int i = 0; i < row; i++) {
            for (int j = row - i; j > 0; j--) {
                System.out.print(".");
            }
            for (int k = 0; k <= i; k++) {
                System.out.print("* ");
            }
            System.out.println();
        }
        for (int i = row; i > 0; i--) {
            for (int j = 0; j <= row - i; j++) {
                System.out.print(".");
            }
            for (int k = i - 1; k > 0; k--) {
                System.out.print(" *");
            }
            System.out.println();
        }
    }
}
