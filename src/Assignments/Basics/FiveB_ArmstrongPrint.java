package Assignments.Basics;

import java.util.Scanner;

public class FiveB_ArmstrongPrint {
    public static Scanner input = new Scanner(System.in);

    public static boolean checker(int pass) {
        int temp, count = 0, ans = 0, rem;
        temp = pass;

        while (temp > 0) {
            temp /= 10;
            count++;
        }

        temp = pass;

        while (temp > 0) {
            rem = temp % 10;
            ans += (Math.pow(rem, count));
            temp /= 10;
        }

        if (ans == pass) {
            return true;
        } else {
            return false;
        }
    }

    public static void main(String[] args) {
        int limit;

        System.out.println("Enter the limit: ");
        limit = input.nextInt();
        System.out.printf("The Armstrong number upto %d:\n", limit);

        for (int i = 0; i <= limit; i++) {
            if (checker(i)) {
                System.out.println(i);
            }
        }
    }
}
