package Assignments.Basics;

import java.util.Scanner;

public class Two_PrimeNumbers {
    public static Scanner input = new Scanner(System.in);
    public static void main(String[] args) {
        int num, count = 0, middle;

        System.out.println("Enter any number: ");
        num = input.nextInt();

        if (num != 1 && num != 0) {
            if (num != 2) {
                middle = num / 2;
                if (num % 2 == 0) {
                    System.out.println("The number is Composite.");
                } else {
                    for (int i = 1; i <= middle; i++) {
                        if (num % i == 0) {
                            count++;
                            if (count > 2) {
                                System.out.println("The number is Composite.");
                                break;
                            } else {
                                System.out.println("The number is Prime.");
                            }
                        }
                    }
                }
            } else {
                System.out.println("The number is Prime.");
            }
        } else {
            System.out.println("The numbers 0 and 1 are neither Prime nor Composite.");
        }
    }
}
