package Assignments.Basics;

import java.util.Scanner;

public class Four_Factorial {
    public static Scanner input = new Scanner(System.in);

    public static void main(String[] args) {
        int num, ans = 1;

        System.out.println("Enter any number: ");
        num = input.nextInt();

        for (int i = 1; i <= num; i++) {
            ans = ans * i;
        }

        System.out.println("The Factorial of " + num + " is " + ans);
    }
}
