package Assignments.Basics;

import java.util.Scanner;

public class FiveA_ArmstrongFind {
    public static Scanner input = new Scanner(System.in);
    public static void main(String[] args) {
        int num, temp, rem;
        int digits = 0, ans = 0;

        System.out.println("Enter any number: ");
        num = input.nextInt();
        temp = num;

        while (temp > 0) {
            temp /= 10;
            digits++;
        }

        temp = num;

        while (temp > 0) {
            rem = temp % 10;
            ans += (Math.pow(rem, digits));
            temp /= 10;
        }

        if (ans == num) {
            System.out.println("The number is a Armstrong number...");
        } else {
            System.out.println("The number is not a Armstrong number...");
        }

    }
}
