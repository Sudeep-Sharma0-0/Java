package Assignments.Basics;

import java.util.Scanner;

public class Three_Palindrome {
    public static Scanner input = new Scanner(System.in);
    public static void main(String[] args) {
        int reverse = 0;

        System.out.println("Enter a number greater than 0: ");
        int number = input.nextInt();
        int temp = number;

        for (; number != 0; number = number / 10) {
            int remainder = number % 10;
            reverse = reverse * 10 + remainder;
        }

        if (reverse == temp) {
            System.out.println("It is a palindrome number.");
        } else {
            System.out.println("It is not a palindrome number.");
        }
    }
}
