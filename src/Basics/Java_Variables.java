package Basics;

public class Java_Variables {
    int glo = 20; //glo is a instance variable which contains a value: 20
    static int sta = 30; //sta is a static variable which contains a value: 30

    public static void main(String[] args) {
        int var = 10; // var is a local variable which contains a value: 10

        //******************Type Casting******************
        float f = 2.5f;
        int i = 10;

        int i2 = (int)f;
        float f2 = (float)i;

        System.out.println("f: " + f + "\ni: " + i + "\nf2: " + f2 + "\ni2: " + i2);
    }
}
