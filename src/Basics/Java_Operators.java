package Basics;

public class Java_Operators {
    public static void main(String[] args) {
        int a = 10, b = 50;

        int sum = a + b;    //Addition
        int sub = b - a;    //Subtraction
        int mul = a * b;    //Multiplication
        int div = b / a;    //Division
        int mod = b % a;    //Remainder
        a = ++a;            //Unary Increament
        b = --b;            //Unary Decreament

        //Assignment Operator
        a += 10;
        b -= 10;
        a *= 10;
        b /= 10;

        System.out.println(sum);
        System.out.println(sub);
        System.out.println(mul);
        System.out.println(div);
        System.out.println(mod);
        System.out.println(a);
        System.out.println(b);
    }
}
