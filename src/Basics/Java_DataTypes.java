package Basics;

public class Java_DataTypes {
    public static void main(String[] args) {
        //******************primitive datatypes******************
        boolean bool = false; // boolean
        char cha = 'a';       // character
        byte byt = 10;        // one byte (-128 to 127)
        short sho = 10000;    // short (-32,768 to 32,767)
        int in = 40000;       // int (-2^31 to 2^31-1)
        double dob = 99999999;// double (-2^63 to 2^63-1)
        float flo = 10.83f;   // float includes decimal points

        //******************non primitive datatypes******************
        String str = "hello world";     // string
        int arr[] = {1, 2, 3, 4};       // array

        System.out.println(bool);
        System.out.println(cha);
        System.out.println(byt);
        System.out.println(sho);
        System.out.println(in);
        System.out.println(dob);
        System.out.println(flo);
        System.out.println(str);
        System.out.println(arr[0]);
    }
}
